from driver_base import DriverBase

import signal
import os
import time
import logging
import sys
from pydispatch import dispatcher

# MQTT
import json
import paho.mqtt.client as mqtt


logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=int(os.environ.get('LOG_LEVEL', 50)),
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)

logger = logging.getLogger("MQTTDriver")
logging.getLogger("chardet.charsetprober").disabled = True

'''
Please paste this json into the Driver's devices config

[{
	"description": "Dual controllable power point",
	"name": "ArlecTwin Power Point1",
	"talents": [{
		"command": "POWER1",
		"description": "",
		"name": "Lamp1",
		"style": "bool",
		"values": ["OFF", "ON"]
	}, {
		"command": "POWER2",
		"description": "",
		"name": "Lamp2",
		"style": "bool",
		"values": ["OFF", "ON"]
	}],
	"unique_id": "ArlecTwin1"
}, {
	"description": "Dual controllable power point",
	"name": "ArlecTwin Power Point2",
	"talents": [{
		"command": "POWER1",
		"description": "",
		"name": "String Lights",
		"style": "bool",
		"values": ["OFF", "ON"]
	}, {
		"command": "POWER2",
		"description": "",
		"name": "Globe Lights",
		"style": "bool",
		"values": ["OFF", "ON"]
	}],
	"unique_id": "ArlecTwin2"
}, {
	"description": "Dual controllable power point",
	"name": "ArlecTwin Power Point3",
	"talents": [{
		"command": "POWER1",
		"description": "",
		"name": "Spare",
		"style": "bool",
		"values": ["OFF", "ON"]
	}, {
		"command": "POWER2",
		"description": "",
		"name": "Heater",
		"style": "bool",
		"values": ["OFF", "ON"]
	}],
	"unique_id": "ArlecTwin3"
}, {
	"description": "Fan and Light controller",
	"name": "Front Room Fan Control",
	"talents": [{
		"command": "POWER1",
		"description": "Switches the lamp on and off",
		"name": "Celing Light",
		"style": "bool",
		"values": ["OFF", "ON"]
	}, {
		"command": "POWER2",
		"description": "Switches the fan on and off",
		"name": "Fan",
		"style": "bool",
		"values": ["OFF", "ON"]
	}, {
		"command": "EVENT",
		"description": "Toggles the fan speed",
		"name": "Speed",
		"style": "momentary",
		"values": ["NEXTSPEED"]
	}],
	"unique_id": "Fan1"
}, {
	"description": "IR Bridge",
	"unique_id": "IRBridge",
	"name": "IR Bridge Controller",
	"talents": [{
		"command": "Power",
		"description": "Switches the AC On and Off",
		"name": "AC Power",
		"style": "hvac",
		"values": ["OFF", "ON"],
		"vendor": "PANASONIC_AC",
                "mode": "Heat"
	}, {
		"command": "Temp",
		"description": "Set temp on AC",
		"name": "Temp",
		"is_range": true,
		"range_min": 16,
		"range_max": 30,
		"requested_range_value": 24,
		"style": "hvac",
		"vendor": "PANASONIC_AC",
                "mode": "Heat"
	}]
}]

'''


class MQTTDevice:
    def __init__(self):
        self.name = None
        self.online = False


class MQTTDriver(DriverBase):
    def __init__(self):
        self.mqtt_connnected = False
        self.stored_online_status = {}
        self.driverconfig = None
        self.mqtt_devices = {}
        self.start_mqtt()
        super().__init__()

    def on_updated_driverconfig(self, config):
        self.driverconfig = config

    def on_talent_change(self, talent):
        local_device = self.get_device_by_unique_id(unique_id=talent.device.unique_id)
        local_talent = local_device.get_talent_by_id(id=talent.id)
        if talent.set_on:
            self.talent_clear(talent=talent, clear='set_on', in_transition=True)
            if local_talent.style == 'bool':
                self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/{local_talent.command}", local_talent.values[talent.set_on])
                logger.debug(f"Sending {local_device.unique_id}/{local_talent.command} {local_talent.values[talent.set_on]}")

            if local_talent.style == 'momentary':
                self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/{local_talent.command}", local_talent.values[0])
                self.talent_update(talent=talent, in_transition=False)
            if local_talent.style == 'hvac':
                if local_talent.command == "Power":
                    logger.debug(f"Sending {local_device.unique_id}/{local_talent.command} {local_talent.values[talent.set_on]}")

                    ac_command = {"Vendor": local_talent.vendor,
                                  "Model": 0,
                                  "Mode": local_talent.mode,
                                  "Power": local_talent.values[talent.set_on],
                                  "Celsius": "On",
                                  "Temp": 24,
                                  "FanSpeed": "Auto",
                                  "SwingV": "Auto",
                                  "SwingH": "Auto",
                                  "Quiet": "Off",
                                  "Turbo": "Off",
                                  "Econo": "Off",
                                  "Light": "Off",
                                  "Filter": "Off",
                                  "Clean": "Off",
                                  "Beep": "Off",
                                  "Sleep": -1}

                    self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/irhvac", json.dumps(ac_command))
                    logger.debug(f"Patching talent to {talent.set_on} {talent.id}")

        if talent.set_off:
            self.talent_clear(talent=talent, clear='set_off', in_transition=True)
            if local_talent.style == 'bool':
                self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/{local_talent.command}", local_talent.values[not talent.set_off])
                logger.debug(f"Sending {local_device.unique_id}/{local_talent.command} {local_talent.values[not talent.set_off]}")

            if local_talent.style == 'hvac':
                if local_talent.command == "Power":
                    logger.debug(f"Sending {local_device.unique_id}/{local_talent.command} {local_talent.values[not talent.set_off]}")

                    ac_command = {"Vendor": local_talent.vendor,
                                  "Model": 0,
                                  "Mode": "Off",
                                  "Power": local_talent.values[not talent.set_off],
                                  "Celsius": "On",
                                  "Temp": 24,
                                  "FanSpeed": "Auto",
                                  "SwingV": "Auto",
                                  "SwingH": "Auto",
                                  "Quiet": "Off",
                                  "Turbo": "Off",
                                  "Econo": "Off",
                                  "Light": "Off",
                                  "Filter": "Off",
                                  "Clean": "Off",
                                  "Beep": "Off",
                                  "Sleep": -1}

                    self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/irhvac", json.dumps(ac_command))
                    logger.debug(f"Patching talent to {talent.set_on} {talent.id}")

        if talent.requested_range_value != talent.range_value:
            if local_talent.style == 'hvac':
                if local_talent.command == "Temp":
                    logger.debug(f"Sending {local_device.unique_id}/{local_talent.command} {talent.requested_range_value}")

                    ac_command = {"Vendor": local_talent.vendor,
                                  "Model": 0,
                                  "Mode": local_talent.mode,
                                  "Power": "On",
                                  "Celsius": "On",
                                  "Temp": talent.requested_range_value,
                                  "FanSpeed": "Auto",
                                  "SwingV": "Auto",
                                  "SwingH": "Auto",
                                  "Quiet": "Off",
                                  "Turbo": "Off",
                                  "Econo": "Off",
                                  "Light": "Off",
                                  "Filter": "Off",
                                  "Clean": "Off",
                                  "Beep": "Off",
                                  "Sleep": -1}

                    self.mqtt_client.publish(f"zenav/cmnd/{local_device.unique_id}/irhvac", json.dumps(ac_command))

    def start_mqtt(self):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_connect = self.on_mqtt_connect
        self.mqtt_client.on_message = self.on_mqtt_message
        self.mqtt_client.on_disconnect = self.on_mqtt_disconnect
        self.mqtt_client.username_pw_set(username=os.environ.get('MQTT_USERNAME', 'no user set'),
                                         password=os.environ.get('MQTT_PASSWORD', 'no password set'))

        try:
            self.mqtt_client.connect(os.environ.get('MQTT_SERVER', 'localhost'),
                                     int(os.environ.get('MQTT_PORT', '1883')),
                                     os.environ.get('MQTT_TIMEOUT', 60))
        except Exception as error:
            logger.critical(f"environment variable MQTT_SERVER: {os.environ.get('MQTT_SERVER', 'localhost')}")
            logger.critical(f"environment variable MQTT_PORT: {os.environ.get('MQTT_PORT', '1883')}")
            logger.critical(f"Unable to start mqtt client: {error}")
            quit()
            return
        self.mqtt_client.loop_start()

    def on_mqtt_disconnect(self, client, userdata, rc):
        self.mqtt_connnected = False
        logger.critical(f"mqtt client disconnected with result code: {rc}")
        for device in self.driverbaseconf.devices:
            self.device_update(device=device, online=False)

    def on_mqtt_connect(self, client, userdata, flags, rc):
        logger.debug(f"Connected with result code {rc}")
        self.mqtt_connnected = True
        # for device in self.conf.devices:
        #     self.on_device_update(device.db_id, True)
        # for device in self.conf.devices:
        #     self.mqtt_client.subscribe(f"zenav/tele/{device.unique_id}")
        self.mqtt_client.subscribe(f"zenav/#")

    def on_mqtt_message(self, client, userdata, msg):
        topic = msg.topic.split('/')
        logger.debug(f"{topic} {str(msg.payload)}")

        local_device = self.get_device_by_unique_id(topic[2])
        if topic[2] not in self.mqtt_devices:
            self.mqtt_devices[topic[2]] = {"online": False}

        # Find if it is our device
        # if not local_device:
        #     logger.debug(f"Storing MQTT message from unknown device {topic[2]}")
        #     return

        if 'tele' in msg.topic:
            if 'LWT' in msg.topic:
                # LWT -- shows if device is online or offline
                # Need to update online for device
                if b'Online' in msg.payload:
                    logger.debug(f"Got online")
                    self.mqtt_devices[topic[2]]['online'] = True
                    # self.stored_online_status[topic[2]] = True
                    if not local_device:
                        return
                    if not local_device.online:
                        local_device.online = True
                        self.device_update(device=local_device, online=True)

                elif b'Offline' in msg.payload:
                    logger.debug(f"Got offline")
                    self.mqtt_devices[topic[2]]['online'] = False
                    # self.stored_online_status[topic[2]] = False
                    if not local_device:
                        return
                    if local_device.online:
                        local_device.online = False
                        self.device_update(device=local_device, online=False)
                else:
                    logger.debug(f"Got LWT, but not sure what to do: {msg.payload.decode()}")
                return

            # zenav/tele/IRBridge/RESULT = {"IrReceived":{"Protocol":"PANASONIC_AC","Bits":216,"Data":"0x0x0220E004000000060220E00400393C80AF0D000EE0000081000026","Repeat":0,"IRHVAC":{"Vendor":"PANASONIC_AC","Model":0,"Mode":"Cool","Power":"On","Celsius":"On","Temp":30,"FanSpeed":"Auto","SwingV":"Auto","SwingH":"Auto","Quiet":"Off","Turbo":"Off","Econo":"Off","Light":"Off","Filter":"Off","Clean":"Off","Beep":"Off","Sleep":-1}}}
            if 'RESULT' in msg.topic:
                payload = json.loads(msg.payload)
                if not local_device:
                    return
                for local_talent in local_device.talents:
                    if local_talent.style == 'hvac':
                        # logger.critical(f"Checking for local_talent.command: {local_talent.command} is in payload['IrReceived']['IRHVAC']: {payload['IrReceived']['IRHVAC']}")
                        if 'IRHVAC' in payload['IrReceived']:
                            if local_talent.command in payload['IrReceived']['IRHVAC']:
                                if local_talent.command == 'Power':
                                    # logger.critical(f"Got IRHVAC result: {payload['IrReceived']['IRHVAC']} payload: {payload['IrReceived']['IRHVAC'][local_talent.command]} state = {(payload['IrReceived']['IRHVAC'][local_talent.command] == 'On')}")
                                    state = (payload['IrReceived']['IRHVAC'][local_talent.command] == 'On')
                                    if local_talent.state != state:
                                        logger.debug(f"Setting {local_talent} {state}")
                                        local_talent.state = state
                                        self.talent_update(talent=local_talent,
                                                           state=local_talent.state,
                                                           in_transition=False)
                                if local_talent.command == "Temp":
                                    # logger.critical(f"Got IRHVAC result: {payload['IrReceived']['IRHVAC']} payload: {payload['IrReceived']['IRHVAC'][local_talent.command]}")
                                    range_value = payload['IrReceived']['IRHVAC'][local_talent.command]
                                    if local_talent.range_value != range_value:
                                        local_talent.range_value = range_value
                                        # Here we have received an update from the IR receiver
                                        # This means someone has send a command to the AC
                                        # We need to reflect this which means we need to set both range_value and requested range value
                                        # So I can not use talent_update
                                        self._ws_send(stream="talentstream",
                                                      payload={"action": "patch",
                                                               "data": {"in_transition": False,
                                                                        "range_value": local_talent.range_value,
                                                                        "requested_range_value": local_talent.range_value},
                                                               "pk": local_talent.id,
                                                               "request_id": self._get_request_id()})

            # {"Time":"2020-12-01T08:02:22","Uptime":"0T09:15:14","UptimeSec":33314,"Heap":31,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":0,"POWER1":"OFF","POWER2":"OFF","Wifi":{"AP":1,"SSId":"BigPondE9736F","BSSId":"CC:40:D0:79:3E:23","Channel":9,"RSSI":60,"Signal":-70,"LinkCount":1,"Downtime":"0T00:00:08
            if 'STATE' in msg.topic:
                payload = json.loads(msg.payload)
                logger.debug(f"Got STATE message:  {payload}")
                # state messages contain the state of all talents in them
                if not local_device:
                    return
                for local_talent in local_device.talents:
                    if local_talent.command in payload:
                        state = (payload[local_talent.command] == 'ON')
                        self.mqtt_devices[topic[2]][local_talent.command] = state
                        # Check if we need to update
                        # Since we get this message all the time even if state hasn't changed
                        if local_talent.state != state:
                            self.talent_update(talent=local_talent, state=state)

        # zenav/stat/ArlecTwin/RESULT b'{"POWER1":"OFF"}'
        if 'stat' in msg.topic:
            if 'RESULT' in msg.topic:
                payload = json.loads(msg.payload)
                logger.debug(f"Got stat message:  {payload}")
                if not local_device:
                    return
                for local_talent in local_device.talents:
                    logger.debug(f"Checking style: {local_talent.style}")
                    if local_talent.style == 'bool':
                        if local_talent.command in payload:
                            state = (payload[local_talent.command] == 'ON')
                            self.mqtt_devices[topic[2]][local_talent.command] = state
                            # Do not check if state has changed from the web,
                            # We need to update no matter what here to ensure transiton gets cleared.
                            self.talent_update(talent=local_talent,
                                               state=state,
                                               in_transition=False)
                    # # 23:59:01 MQT: zenav/stat/IRBridge/RESULT = {"IRHVAC":{"Vendor":"PANASONIC_AC","Model":-1,"Mode":"Cool","Power":"On","Celsius":"On","Temp":27,"FanSpeed":"Auto","SwingV":"Auto","SwingH":"Auto","Quiet":"Off","Turbo":"Off","Econo":"Off","Light":"Off","Filter":"Off","Clean":"Off","Beep":"Off","Sleep":-1}}
                    # if local_talent.style == 'hvac':
                    #     logger.critical(f"Checking for local_talent.command: {local_talent.command} is in payload['IrReceived']['IRHVAC']: {payload['IrReceived']['IRHVAC']}")
                    #     if local_talent.command in payload['IrReceived']['IRHVAC']:
                    #         logger.critical(f"Got IRHVAC result: {payload['IrReceived']['IRHVAC']} payload: {payload['IrReceived']['IRHVAC'][local_talent.command]} state = {(payload['IRHVAC'][local_talent.command] == 'On')}")
                    #         state = (payload['IrReceived']['IRHVAC'][local_talent.command] == 'On')
                    #         if local_talent.state != state:
                    #             logger.debug(f"Setting {local_talent} {state}")
                    #             local_talent.state = state
                    #             self.talent_update(talent=local_talent,
                    #                                   state=local_talent.state,
                    #                                   in_transition=False)

    def periodic(self):
        # This will be call once a second by DriverBase

        # We need to sync web with any stored information
        for mqtt_device in self.mqtt_devices:
            web_device = self.get_device_by_unique_id(mqtt_device)
            if web_device:
                if web_device.online != self.mqtt_devices[mqtt_device]['online']:
                    self.device_update(device=web_device, online=self.mqtt_devices[mqtt_device]['online'])
                for talent in web_device.talents:
                    if talent.name == mqtt_device and 'state' in mqtt_device:
                        self.talent_update(talent=talent, state=mqtt_device['state'])


class ServerExit(Exception):
    pass


def service_shutdown(signum, frame):
    print('Caught signal %d' % signum)
    raise ServerExit


def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    my_driver = MQTTDriver()
    my_driver.daemon = True
    my_driver.start()
    try:
        while True:
            time.sleep(1)
    except ServerExit:
        logger.critical('Shutting down')
        dispatcher.send(signal="Shutdown")
        my_driver.join()


if __name__ == '__main__':
    main()
