from mqtt_driver import MQTTDriver, Device, Talent
from unittest.mock import patch, Mock
import unittest
import datastore


# @patch('mqtt_driver.MQTTDriver.mqtt_client')
@patch('mqtt_driver.MQTTDriver.ws_send')
class TestReactions(unittest.TestCase):

    def setUp(self):
        my_conf = datastore.Config()
        my_conf.driver_name = "MQTTDriver"
        user_conf = [{'name': 'ArlecTwin Power Point1',
                      'description': 'Dual controllable power point',
                      'unique_id': 'ArlecTwin1',
                      'talents': [{'name': 'Lamp1',
                                   'command': 'POWER1',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  {'name': 'Lamp2',
                                   'command': 'POWER2',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  ]
                      },
                     {'name': 'ArlecTwin Power Point2',
                      'description': 'Dual controllable power point',
                      'unique_id': 'ArlecTwin2',
                      'talents': [{'name': 'Christmas Tree',
                                   'command': 'POWER1',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  {'name': 'Plug 2',
                                   'command': 'POWER2',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  ]
                      }]
        # Add any new devices
        for conf in user_conf:
            if conf['unique_id'] not in [device.unique_id for device in my_conf.devices]:
                my_conf.devices.append(Device(conf))
        self.my_driver = MQTTDriver(my_conf)
        # print(self.my_driver.conf.devices[0].talents[0].state)
        # my_driver.conf.device_online = True
        setattr(self.my_driver.conf.devices[0], 'db_id', 111)
        setattr(self.my_driver.conf.devices[0].talents[0], 'db_id', 222)
        setattr(self.my_driver.conf.devices[0].talents[1], 'db_id', 333)

    def test_device_online_changes_with_LWT_messages(self, mock_ws_send):
        # Test LWT
        msg = Mock()
        msg.topic = 'zenav/tele/ArlecTwin1/LWT'
        msg.payload = b'Online'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        self.assertTrue(mock_ws_send.called)
        name, args, kwargs = mock_ws_send.mock_calls[0]
        self.assertEqual(kwargs['stream'], 'devicestream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'is_online': True},
                                             "pk": 111,
                                             "request_id": 42})

        # now test offline
        msg.topic = 'zenav/tele/ArlecTwin1/LWT'
        msg.payload = b'Offline'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        self.assertTrue(mock_ws_send.called)
        name, args, kwargs = mock_ws_send.mock_calls[1]
        self.assertEqual(kwargs['stream'], 'devicestream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'is_online': False},
                                             "pk": 111,
                                             "request_id": 42})

    def test_state_changes_with_STATE_msg(self, mock_ws_send):
        self.my_driver.conf.devices[0].talents[0].state = False
        self.my_driver.conf.devices[0].talents[1].state = False
        msg = Mock()
        msg.topic = 'zenav/tele/ArlecTwin1/STATE'
        msg.payload = b'{"Time":"2020-10-23T20:25:08","Uptime":"0T20:00:18","UptimeSec":72018,"Heap":30,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":2,"POWER1":"ON","POWER2":"ON","Wifi":{"AP":1,"SSId":"BigPondE9736F","BSSId":"CC:40:D0:79:3E:23","Channel":13,"RSSI":100,"Signal":-44,"LinkCount":1,"Downtime":"0T00:00:08"}}'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        # Message should generate 2 calls one of power1 and another for power2
        self.assertTrue(mock_ws_send.called)
        name, args, kwargs = mock_ws_send.mock_calls[0]
        self.assertEqual(kwargs['stream'], 'talentstream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'state': True,
                                                      'in_transition': False},
                                             "pk": 222,
                                             "request_id": 42})
        name, args, kwargs = mock_ws_send.mock_calls[1]
        self.assertEqual(kwargs['stream'], 'talentstream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'in_transition': False,
                                                      'state': True},
                                             "pk": 333,
                                             "request_id": 42})

        # if we send it again nothing should change
        msg.topic = 'zenav/tele/ArlecTwin1/STATE'
        msg.payload = b'{"Time":"2020-10-23T20:25:08","Uptime":"0T20:00:18","UptimeSec":72018,"Heap":30,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":2,"POWER1":"ON","POWER2":"ON","Wifi":{"AP":1,"SSId":"BigPondE9736F","BSSId":"CC:40:D0:79:3E:23","Channel":13,"RSSI":100,"Signal":-44,"LinkCount":1,"Downtime":"0T00:00:08"}}'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        self.assertEqual(len(mock_ws_send.mock_calls), 2)

        # if we send it with a single change we should see one message go out
        msg.topic = 'zenav/tele/ArlecTwin1/STATE'
        msg.payload = b'{"Time":"2020-10-23T20:25:08","Uptime":"0T20:00:18","UptimeSec":72018,"Heap":30,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":2,"POWER1":"ON","POWER2":"OFF","Wifi":{"AP":1,"SSId":"BigPondE9736F","BSSId":"CC:40:D0:79:3E:23","Channel":13,"RSSI":100,"Signal":-44,"LinkCount":1,"Downtime":"0T00:00:08"}}'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        self.assertEqual(len(mock_ws_send.mock_calls), 3)

    def test_state_changes_with_RESULT_msg(self, mock_ws_send):
        msg = Mock()
        msg.topic = 'zenav/tele/ArlecTwin1/STATE'
        msg.payload = b'{"Time":"2020-10-23T20:25:08","Uptime":"0T20:00:18","UptimeSec":72018,"Heap":30,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":2,"POWER1":"ON","POWER2":"ON","Wifi":{"AP":1,"SSId":"BigPondE9736F","BSSId":"CC:40:D0:79:3E:23","Channel":13,"RSSI":100,"Signal":-44,"LinkCount":1,"Downtime":"0T00:00:08"}}'
        self.my_driver.on_mqtt_message(client=None, userdata=None, msg=msg)
        # Message should generate 2 calls one of power1 and another for power2
        self.assertTrue(mock_ws_send.called)
        name, args, kwargs = mock_ws_send.mock_calls[0]
        self.assertEqual(kwargs['stream'], 'talentstream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'in_transition': False,
                                                      'state': True},
                                             "pk": 222,
                                             "request_id": 42})
        name, args, kwargs = mock_ws_send.mock_calls[1]
        self.assertEqual(kwargs['stream'], 'talentstream')
        self.assertEqual(kwargs['payload'], {"action": "patch",
                                             "data": {'in_transition': False,
                                                      'state': True},
                                             "pk": 333,
                                             "request_id": 42})

    def test_incoming_driver_update_does_trigger_response(self, mock_ws_send):
        message = '{"stream": "driverstream", "payload": {"errors": [], "data": {"name": "MQTTDriver", "owner": "mqtt_user", "description": "", "is_online": true, "authorized": true, "id": 111, "driver_uuid": "067d1272-2d33-4427-822e-898b456756a7", "devices": [45]}, "action": "update", "response_status": 200, "request_id": 42}}'
        self.my_driver.ws_incoming(message)
        self.assertFalse(mock_ws_send.called)

