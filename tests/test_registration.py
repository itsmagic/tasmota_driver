from mqtt_driver import MQTTDriver, Device, Talent
from unittest.mock import patch, Mock
import unittest
import datastore


@patch('mqtt_driver.MQTTDriver.ws_send')
class TestReactions(unittest.TestCase):

    def setUp(self):
        my_conf = datastore.Config()
        my_conf.driver_name = "MQTTDriver"
        user_conf = [{'name': 'ArlecTwin Power Point1',
                      'description': 'Dual controllable power point',
                      'unique_id': 'ArlecTwin1',
                      'talents': [{'name': 'Lamp1',
                                   'command': 'POWER1',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  {'name': 'Lamp2',
                                   'command': 'POWER2',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  ]
                      },
                     {'name': 'ArlecTwin Power Point2',
                      'description': 'Dual controllable power point',
                      'unique_id': 'ArlecTwin2',
                      'talents': [{'name': 'Christmas Tree',
                                   'command': 'POWER1',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  {'name': 'Plug 2',
                                   'command': 'POWER2',
                                   'style': 'bool',
                                   'values': ['OFF', 'ON'],
                                   'description': f''},
                                  ]
                      }]
        # Add any new devices
        for conf in user_conf:
            if conf['unique_id'] not in [device.unique_id for device in my_conf.devices]:
                my_conf.devices.append(Device(conf))
        self.my_driver = MQTTDriver(my_conf)
        # print(self.my_driver.conf.devices[0].talents[0].state)
        # my_driver.conf.device_online = True
        setattr(self.my_driver.conf.devices[0], 'db_id', 111)
        setattr(self.my_driver.conf.devices[0].talents[0], 'db_id', 222)
        setattr(self.my_driver.conf.devices[0].talents[1], 'db_id', 333)

