FROM python:3

WORKDIR /code

COPY requirements.txt /code/
COPY demo_shelve_data /code/shelve_data
RUN pip install --upgrade -r requirements.txt

COPY . /code/

CMD ["mqtt_driver.py"]
ENTRYPOINT ["python3"]